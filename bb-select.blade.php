<div {!! $attributes->__toString() !!}
     x-cloak
     {{--x-data="BBSelect({ el:$el, lwModel:@entangle($attributes->wire('model')) })" @click.away="closeListBox()" @keydown.escape="closeListBox()">--}}
     x-data="BBSelect({ el:$el, lwModel:@entangle($attributes['data-lw-model'])})" @click.away="closeListBox()"
     @keydown.escape="closeListBox()">
    <div class="position-relative">
        <div x-ref="selector" class=""
             @click="toggleListboxVisibility()"
             @keydown.enter.stop="selectOption()" @keydown.arrow-up.stop="focusPreviousOption()"
             @keydown.arrow-down.stop="focusNextOption()" :aria-expanded="open" aria-haspopup="listbox">
            <div>
                <div class="form-control d-flex justify-content-between">

                    <div x-show="!isMultiple" x-cloak>
                        <div class="d-flex align-items-center">
                            <span x-show="Object.keys(value)[0]" @click.stop="unSelectOption(Object.keys(value)[0])" class="me-2 tag-remove text-muted">X</span>
                            <span x-text="Object.keys(value)[0] ? value[Object.keys(value)[0]] : placeholder"></span>
                        </div>
                    </div>

                    <div x-show="isMultiple">
                        <div class="d-flex align-items-center flex-wrap">
                                <span class="text-muted" x-show="Object.keys(value).length === 0"
                                      x-text="placeholder"></span>

                            <template x-for="(option, key) in value" :key="'tag-' + key">
                                <div class="my-2 px-2 py-2 me-2 badge bg-info d-flex align-items-center">
                                    <span @click.stop="" x-text="option"></span>
                                    <span @click.stop="unSelectOption(key)" class="ms-2 tag-remove">X</span>
                                </div>
                            </template>
                        </div>
                    </div>

                    <svg class="" viewBox=" 0 0 20 20" fill="none" stroke="currentColor" style="width: 25px;">
                        <path d="M7 7l3-3 3 3m0 6l-3 3-3-3" stroke-width="1.5" stroke-linecap="round"
                              stroke-linejoin="round"/>
                    </svg>
                </div>


            </div>
        </div>
        <div class="itemsListContainer position-absolute w-100 mt-1 bg-white rounded-md shadow-lg rounded-3 p-3"
             style="display:none; z-index:9;" x-show="open" x-cloak>
            <div>
                <input x-show="searchEnabled" x-ref="q" x-model.debounce.250ms="q"
                       x-bind:placeholder="searchPlaceholder"
                       @keydown.enter.stop.prevent="selectOption()"
                       @keydown.arrow-up.stop.prevent="focusPreviousOption()"
                       @keydown.arrow-down.stop.prevent="focusNextOption()" type="search" class="form-control mb-3"/>

                <div class="ms-2" x-show="isSearching">
                    <div class="d-flex align-items-center">
                        <small class="text-muted" x-text="searchingText"></small>
                        <div class="ms-2 spinner-border spinner-border-sm" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                </div>

                <div class="text-sm text-muted ms-2" x-show="!isSearching && emptyResults"
                     x-text="emptyOptionsMessage"></div>

                <div x-ref="itemsList" class="mt-1"
                     style="max-height: 200px; overflow: auto; scrollbar-width: thin;">
                    <ul x-ref="listbox" class="m-0 p-0 shadow-xs" role="listbox">
                        <template x-for="(key, index) in Object.keys(options)" :key="key">
                            <li x-show="( (isMultiple && !isSelected(key)) || !isMultiple)" @click="selectOption()"
                                @mouseenter="focusedOptionIndex = index"
                                @mouseleave="focusedOptionIndex = null"
                                :class="{ 'item-list-hover' : index === focusedOptionIndex && !isSelected(key), 'item-list-selected' : isSelected(key) }"
                                class="item-list rounded-1 py-2 ps-2 pe-3 me-2" role="option">
                                <div class="d-flex justify-content-between">
                                    <span class="" x-text=" Object.values(options)[index]"></span>
                                    <span x-show="isSelected(key)" class="" style=" width: 25px; color: #6e8e64;">
                                        <svg class="" viewBox=" 0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd"
                                                  d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                  clip-rule="evenodd"/>
                                        </svg>
                                    </span>
                                </div>
                            </li>
                        </template>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@once

    @push('scripts')
        <script>
            function BBSelect(config) {

                let el = config.el.dataset;

                return {

                    data: JSON.parse(el.options),
                    options: JSON.parse(el.options),
                    placeholder: el.placeholder ?? 'Select an option',
                    emptyOptionsMessage: el.emptyOptionMessage ?? 'No results match your search',

                    name: el.name,
                    value: {},
                    lwValue: config.lwModel,

                    q: '',
                    focusedOptionIndex: null,
                    open: false,
                    emptyResults: false,
                    searchEnabled: ((el.searchEnabled === 'true') || (el.remoteEnabled === 'true')),
                    searchPlaceholder: (el.searchPlaceholder) ?? 'Search',
                    isMultiple: (el.multiple === 'true'),

                    //REMOTE ATTRIBUTES
                    useRemoteData: (el.remoteEnabled === 'true'),
                    remoteEndpoint: el.remoteEndpoint ?? null,
                    remoteQueryFieldName: el.remoteQueryField ?? 'q',
                    searchingText: el.remoteSearchingText ?? 'Searching',
                    isSearching: false,

                    init: function () {

                        if (this.isMultiple) {
                            for (const [key, value] of Object.entries(this.lwValue)) {
                                this.value[value] = this.data[value];
                            }
                        } else {
                            this.value = {}
                            this.value[this.lwValue] = this.data[this.lwValue];
                        }

                        //REMOTE DATA
                        if (this.useRemoteData) {
                            this.data = {}
                            this.options = {}
                        }


                        this.$watch('q', (value, oldValue) => {

                            if (value === oldValue)
                                return;

                            this.focusedOptionIndex = null;
                            this.resetListScroll();


                            if (this.useRemoteData && this.remoteEndpoint !== null) {

                                if (!this.open || !value) {
                                    return
                                }
                                this.isSearching = true;
                                fetch(this.remoteEndpoint + "?" + this.remoteQueryFieldName + "=" + value).then(
                                    response => response.json()).then(data => {
                                    this.data = data
                                    this.options = data
                                    this.isSearching = false;

                                    this.emptyResults = (Object.keys(data).length === 0);

                                });
                            } else {

                                if (!this.open || !value) {
                                    return this.options = this.data;
                                }

                                this.options = Object.keys(this.data)
                                    .filter((key) => this.data[key].toLowerCase().includes(value.toLowerCase()))
                                    .reduce((options, key) => {
                                        options[key] = this.data[key]
                                        return options
                                    }, {})

                                this.emptyResults = (Object.keys(this.options).length === 0);

                            }
                        })

                    },

                    toggleListboxVisibility: function () {

                        if (this.open)
                            return this.closeListBox();

                        let valueIndex = Object.keys(this.value)[0];
                        this.focusedOptionIndex = Object.keys(this.options).indexOf(valueIndex);

                        if (this.focusedOptionIndex < 0) {
                            this.focusedOptionIndex = 0;
                        }

                        this.open = true;

                        this.$nextTick(() => {

                            let component = this;
                            if (this.searchEnabled) {

                                setTimeout(() => {
                                    component.$refs.q.focus();
                                }, 300)
                            }

                            if (!this.isMultiple) {
                                this.$refs.listbox.children[this.focusedOptionIndex].scrollIntoView({
                                    block: 'center'
                                });
                            }
                        })
                    },

                    closeListBox: function () {
                        this.open = false;
                        this.focusedOptionIndex = null;
                        this.q = '';
                        this.emptyResults = false;

                        if (this.isMultiple) {
                            this.resetListScroll();
                        }
                    },

                    resetListScroll() {
                        this.$refs.itemsList.scrollTop = 0;
                    },

                    selectOption: function () {
                        if (!this.open) return this.toggleListboxVisibility();
                        let value = Object.keys(this.options)[this.focusedOptionIndex];

                        if (this.isMultiple) {
                            this.value[value] = this.data[value];

                        } else {

                            this.value = {}
                            this.value[value] = this.data[value];

                        }
                        this.refreshLwModel();
                        this.$dispatch('changed', {value: this.lwValue})
                        this.closeListBox();
                    },

                    refreshLwModel() {
                        if(this.isMultiple) {
                            this.lwValue = Object.keys(this.value);
                        }
                        else {
                            this.lwValue = Object.keys(this.value)[0];
                        }
                    },

                    unSelectOption(key) {
                        delete (this.value[key]);
                        this.refreshLwModel();

                    },

                    isSelected(key) {
                        return this.value[key]
                    },

                    focusNextOption: function () {

                        if (this.focusedOptionIndex === null) {
                            return this.focusedOptionIndex = 0;
                        }

                        if (this.focusedOptionIndex + 1 >= Object.keys(this.options).length) return;
                        this.focusedOptionIndex++;

                        this.$refs.listbox.children[this.focusedOptionIndex].scrollIntoView({
                            block: 'center'
                        });
                    },

                    focusPreviousOption: function () {

                        if (this.focusedOptionIndex - 1 < 0) return;

                        this.focusedOptionIndex--;

                        this.$refs.listbox.children[this.focusedOptionIndex].scrollIntoView({
                            block: 'center'
                        });

                    },
                }

            }
        </script>
    @endpush
@endonce

<style>
    .tag-remove {
        font-weight: bold;
    }

    .tag-remove:hover {
        cursor: pointer;
    }

    .item-list-hover {
        background-color: #f4f4f4 !important;
    }

    .item-list-selected {

        background-color: #f4f2f2 !important;
        color: black;
    }

</style>
